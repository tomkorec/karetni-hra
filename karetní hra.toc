\babel@toc {czech}{}
\contentsline {chapter}{\numberline {1}Obecně}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Princip}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Cíl}{2}{section.1.2}%
\contentsline {chapter}{\numberline {2}Herní pomůcky}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Hrací kostka}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Karty}{3}{section.2.2}%
\contentsline {chapter}{\numberline {3}Herní systém}{4}{chapter.3}%
\contentsline {section}{\numberline {3.1}Karty}{4}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Karetní taxonomie}{4}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Postavy}{4}{subsection.3.1.2}%
\contentsline {chapter}{\numberline {4}Hra}{6}{chapter.4}%
\contentsline {section}{\numberline {4.1}Cesta za pokladem (2-10 hráčů)}{6}{section.4.1}%
\contentsline {subsubsection}{Vzdálenost hráčů}{6}{section*.5}%
\contentsline {subsection}{\numberline {4.1.1}Průběh kola}{7}{subsection.4.1.1}%
